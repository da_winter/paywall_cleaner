try {

	function handleDerStandard() {
		// remove popup
		document.getElementsByClassName('paidblocker')?.[0].remove();

		// disable scroll block
		document.documentElement.classList?.remove('blocker');
	}

	// ***************************************
	// 
	
	var enabled = true;
	if (enabled) {
		handleDerStandard();
	}

} catch (e) {
    console.error("[PC]: exception", e);
}