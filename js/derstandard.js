try {

	function handleDerStandard() {

		// disable scroll block
		document.body.classList.remove('tp-modal-open')

		// listen to changes in body element
		const observer = new MutationObserver(function(mutationRecords) {
			mutationRecords.forEach(records => {
				records.addedNodes.forEach(node => {
					// delete popup modal or delete popup backdrop
					if (node.classList?.contains('tp-modal')
						|| node.classList?.contains('tp-backdrop')
					) {
						node.remove();
					}
				});
			});
		});

		// observe the body
		observer.observe(document.body, {subtree: false, childList: true});
	}

	// ***************************************
	// 
	
	var enabled = true;
	if (enabled) {
		handleDerStandard();
	}

} catch (e) {
    console.error("[PC]: exception", e);
}